# easyosx



## Getting started

**step 1.** download easyosx.zip

**step 2.** extract the folder to your desktop. then paste this link in your browser:

https://releases.ubuntu.com/21.10/ubuntu-21.10-desktop-amd64.iso

and donwload ubuntu. then put it in the same place as unetbootin.exe. it could be anywhere in your computer as long as its in the same place as unetbootin.exe

**step 3.** run unetbootin.exe

**step 4.** next to: Diskiamge click the little white circle. then there should be three options to the right of Diskimage...iso.....an empty white box...and a box with three dots..
click the box with three dots. and open ubuntu.iso...(it should be in the same folder you just moved to your desktop). on the bottom of the window, next to Type...there should be a box that 
says USB Drive. click that box and select: Hard Disk. then click ok on the bottom of the window.

**step 5.** restart your commputer dont press anything yet. there will be two options. one being windows...and the other is unetbootin. choose unetbootin with the up and down arrow keys and hit enter. after that there will be a countdown. dont click anything
text will show up with a red background. the text will say. ubuntu.. (spelt you-bun-two). dont press anything. wait till its done. and you will be in linux.

**step 6.** on your keyboard on the bottom left side. between the ctrl key and the alt key is the windows key. press it (dont hold the key) now type: term. there should be an option that says terminal...or something like it. press enter. now, type in the terminal: 

sudo snap install sosumi



**installing macOS**:

**step 7.** it will work its magic after you enter that command..after that a window will pop up. click in it and dont do anything. there will be yellow text counting down. dont click anything. after that there will be a grey screen with one option. press enter. 
you will see a window that says: macOS Utilities. then click continue and go through the installer...after that..enjoy macOS. if you want to full screen the window, press: ctrl, then alt, then the f key. to get out of the window press the: ctrl, then alt, then the g key. 
